/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ed_ut4_ej6_bote;

/**
 *
 * @author ozymandias
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
       private Bote bruguer;
       private int centilitros = 300;
       private int centimos = 200;
       private int metros = 2;

    public Bote getBruguer() {
        return bruguer;
    }

    public void setBruguer(Bote bruguer) {
        this.bruguer = bruguer;
    }

    public int getCentilitros() {
        return centilitros;
    }

    public void setCentilitros(int centilitros) {
        this.centilitros = centilitros;
    }

    public int getCentimos() {
        return centimos;
    }

    public void setCentimos(int centimos) {
        this.centimos = centimos;
    }

    public int getMetros() {
        return metros;
    }

    public void setMetros(int metros) {
        this.metros = metros;
    }
       
    public static void main(String[] args) {
        Bote bruguer;
        int centilitros = 300;
        int centimos = 200;
        int metros = 2;

        bruguer = new Bote(300, 10, "verde", 2, 500);

        opera_Pinta(bruguer, metros);

        opera_Rellena(bruguer, centilitros, centimos);
    }

    private static void opera_Rellena(Bote titan, int centilitros, int centimos) {
        /*Vamos a intentar añadir 300 centilitros al bote, como se supera la capacidad saltará
        una excepción y no se modificará el contenido del depósito que seguirá siendo 280cl*/
        try {
            System.out.println("Vamos a rellenar el bote");
            titan.rellenar(centilitros, centimos);
            System.out.println("Recarga realizada, ahora tiene " + titan.getContenido() + " centilitros en el bote");
        } catch (Exception e) {
            System.out.println("Error al rellenar el bote, el contenido sigue siendo: " + titan.getContenido());

        }
    }

    private static void opera_Pinta(Bote titan, int metros) {
        /*Vamos a pintar 2 metros, como hay sufuiciente pintura la operación tendrá éxito y se
        descontran 20 centilitros del contenido del bote quedan 280cl*/
        try {
            System.out.println("Vamos a pintar");
            titan.pintar(metros);
            System.out.println("En el bote quedan " + titan.getContenido() + " centilitros");
        } catch (Exception e) {
            System.out.println("Error al pintar");
        }
    }

}
